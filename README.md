# wbsurfer

`wbsurfer` is a python program for generating automated spatially varying seed map movies using connectome workbench.

## Demo

![](demo/spiral_loop.mp4)

## Citations

If you use `wbsurfer`, please cite the following:

```
Gordon, E.M., Chauvin, R.J., Van, A.N., Rajesh, A., Nielsen, A., Newbold, D.J., Lynch, C.J., Seider, N.A., 
Krimmel, S.R., Scheidter, K.M., et al. (2023). A somato-cognitive action network alternates with effector regions 
in motor cortex. Nature. 10.1038/s41586-023-05964-2.
```

## Requirements

You will need `connectome workbench (>=1.4.2)`, `ffmpeg`, and probably `gcc` installed.

## Installation

To install, clone this repo:

```
git clone https://gitlab.com/vanandrew/wbsurfer.git
```

and run:

```
# go to whereever the repo is located
cd /path/to/wbsurfer 
# install via pip (--user or virtualenvs are recommended)
pip install ./
```

If you installed using the `--user` flag, you may need to add `~/.local/bin` to your path.

In a virtualenv, you should be able to run `wb_surfer` and `roi_to_border` from the command line by default.

## How to use

> **__NOTE:__** Currently only dconn files are supported for surface traversal. If you pass in a dtseries in dynconn
> mode into your scene, you will get an error.

First, setup your scene file in connectome workbench to your liking. Then place exactly one seed in the scene.
`wb_surfer` will modify this seed location to render the movie so it doesn't matter where you put it as long as it is
present.

Then, you can use `wb_surfer` to generate a movie. You will need a border file to define the path you want it to render.
You can use `roi_to_border` to generate a border file for use with `wb_surfer`. It can generate a spiral or boundary
path around a given parcellation.

Alternatively, you can provide a list of vertices (either surface indexing or cifti indexing) for `wb_surfer` to use as
the path. Between each pair of vertices, `wb_surfer` will interpolate a path using the shortest geodesic path across
the surface.

> **__NOTE:__** MAKE SURE YOUR SCENE FILE HAS EXACTLY ONE SEED. Behavior is undefined if it has more than one seed, and
> you will get an error if it has no seed.

## Usage Examples

The two commands to invoke are `wb_surfer` and `roi_to_border`.

You can use `-h` to get the help for each command. Example usage is below:

```
# I have a scene at /path/to/scene_file.scene containing a scene with the name "scene_name"
# I have a border file defining the path I want to make a movie from at /path/to/border_file.border

# This saves a movie as /path/to/movie.mp4
wb_surfer /path/to/scene_file.scene scene_name -b /path/to/border_file.border /path/to/movie.mp4

# This saves a movie as /path/to/movie.mp4 at 10 fps, 2 loops, without interpolation
wb_surfer /path/to/scene_file.scene scene_name -b /path/to/border_file.border /path/to/movie.mp4 -r 10 -l 2 --skip_interp

# you can also directly pass in the vertices that you want to trace a path over
# if they are not connected wb_surfer will draw the shortest geodesic path across each pair of vertices
wb_surfer /path/to/scene_file.scene scene_name /path/to/movie.mp4 -o CORTEX_LEFT 901 905 910

# cifti indexing (NOTE THIS IS ZERO-INDEXED!!! So subtract 1 from what you see in the workbench UI.)
wb_surfer /path/to/scene_file.scene scene_name /path/to/movie.mp4 -x 15071 16702 17403

# you can change the width/height of the movie using the -w flag and encoding with the -e flag
wb_surfer /path/to/scene_file.scene scene_name /path/to/movie.mp4 -x 15071 16702 17403 -w 1280 720 -e libx265

# roi_to_border

# I have a dlabel file at /path/to/dlabel.nii (this can also be a dtseries)
# I have a gifti file at /path/to/gifti.nii (note that the index you choose should be on the surface you input, else you will get an access error)
# Index of the ROI I want to display

# This generates a boundary path at ROI 100 and saves it in /path/to/boundary.border
roi_to_border /path/to/dlabel /path/to/gifti.nii /path/to/boundary.border -i 100 -t boundary

# This generates a spiral path with 2 cycles at ROI 42 and saves it in /path/to/spiral.border
roi_to_border /path/to/dlabel /path/to/gifti.nii /path/to/spiral.border -i 42 -t spiral
```
