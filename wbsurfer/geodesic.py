"""Exact search for Geodesic paths"""
from typing import List, Union

import numpy as np

from geodesic_chenhan import (  # type: ignore
    CRichModel as RichModel,
    CPoint3D as Point3D,
    CFace as Face,
    CICHWithFurtherPriorityQueue as ICHWithFurtherPriorityQueue,
    EdgePoint,
    CEdge as Edge,
)


class GeodesicPath:
    def __init__(self, vertices: np.ndarray, faces: np.ndarray):
        # store vertices/faces
        self.vertices = vertices
        self.faces = faces

        # create model
        self.bmodel = RichModel()

        # get vertices and faces in proper format
        self.vertices_list = list()
        for v in vertices:
            self.vertices_list.append(Point3D(float(v[0]), float(v[1]), float(v[2])))
        self.faces_list = list()
        for f in faces:
            self.faces_list.append(Face(int(f[0]), int(f[1]), int(f[2])))

        # load mesh into model
        self.bmodel.LoadModel(self.vertices_list, self.faces_list)
        self.bmodel.Preprocess()

        # initialize current source and emethod
        self.current_source = None
        self.emethod = None

    def update_model(self, source: int) -> None:
        if self.current_source != source:
            self.emethod = ICHWithFurtherPriorityQueue(self.bmodel, set([int(source)]))
            self.emethod.Execute()
            self.current_source = source

    def distances(self, source: int) -> np.ndarray:
        self.update_model(source)
        return np.array([d.disUptodate for d in self.emethod.GetVertexDistances()])

    def path(self, source: int, target: int) -> List[EdgePoint]:
        self.update_model(source)
        # get the path from target to source
        path = self.emethod.FindSourceVertex(int(target), [])
        # reverse path to go from source to target
        path.reverse()
        # return path as list of vertex positions
        return path

    def as_vertex_positions(self, path: List[EdgePoint]) -> List[List[float]]:
        # get the vertex positions
        vertex_positions = list()
        for p in path:
            pt = p.Get3DPoint(self.bmodel)
            vertex_positions.append((pt.x, pt.y, pt.z))
        return np.array(vertex_positions)

    def as_edges(self, path: List[EdgePoint]) -> List[Edge]:
        # get edge structs
        edges = list()
        for p in path:
            e = self.bmodel.Edge(p.index)
            edges.append(e)
        return edges
