import os
from pathlib import Path
import xml.etree.ElementTree as ET
from typing import List, Tuple

import numpy as np
import nibabel as nib
from networkx.algorithms.shortest_paths import shortest_path_length, shortest_path
from yaspin import yaspin
from yaspin.spinners import Spinners

from .images import Cifti, Gifti
from .graph import chenhan_shortest_path, ROIGraph


class Border:
    """Object representing a border file, a workbench related file that can store ordered face/vertex information."""

    def __init__(self, structure_name: str, number_of_vertices: int):
        """Creates a Border Object.

        Parameters
        ----------
        structure_name : str
            Name of structure border is defined on. Valid options are "CORTEX_LEFT" or "CORTEX_RIGHT
        number_of_vertices : int
            Number of vertices in the structure.
        """
        # initialize xml structure
        # create the first element of the borderfile xml
        self._borderfile = ET.Element(
            "BorderFile",
            Version="3",
            Structure=structure_name,
            SurfaceNumberOfVertices=str(number_of_vertices),
        )

        # create necessary subelements
        common_attribs = {"Name": "border", "Red": "0", "Green": "0", "Blue": "0"}
        self._class = ET.SubElement(self._borderfile, "Class", attrib=common_attribs)
        self._border = ET.SubElement(self._class, "Border", attrib=common_attribs)
        self._borderpart = ET.SubElement(self._border, "BorderPart", Closed="False")
        self._vertices = ET.SubElement(self._borderpart, "Vertices")
        self._weights = ET.SubElement(self._borderpart, "Weights")

        # add to ElementTree
        self._btree = ET.ElementTree(self._borderfile)

        # set mode to border
        self._mode = "border"

    @classmethod
    def load(cls, filename: str):
        """Load an existing Borderfile

        Parameters
        ----------
        filename : str
            Path to a border or gifti file.
        """
        # create an empty Borderfile
        border = cls("", "")

        # get the borders and store in an array
        if ".border" in filename:
            # get the element tree of the file
            etree = ET.parse(filename)

            # replace the initialized attributres
            border._btree = etree
            border._borderfile = etree.getroot()
            border._class = etree.find(".//Class")
            border._border = etree.find(".//Border")
            border._borderpart = etree.find(".//BorderPart")
            border._vertices = etree.find(".//Vertices")
            border._weights = etree.find(".//Weights")

            border._mode = "border"
        elif ".gii" in filename:  # this is a .gii file we can substitute as a border file
            raise ValueError("This is broken and at a low priority for fixing...")
            border._surface_file = nib.load(filename)
            border._mode = "surface"

        # return the constructed border object
        return border

    @property
    def vertices(self) -> Tuple[List[int], str]:
        """Get the vertices of border file (last index). Vertices are 0-indexed.

        Returns
        -------
        Tuple[List[int], str]
            Returns the vertices of the border file and the surface it is located on.
        """

        if self._mode == "border":
            # get the surface name
            surface = self._borderfile.get("Structure")
            # get triangles in border
            try:
                triangles = self._vertices.text.rstrip().split("\n")
            except AttributeError:
                triangles = list()
            return ([int(t.split(" ")[2]) for t in triangles], surface)
        elif self._mode == "surface":
            # get the surface name
            primary_anat = self._surface_file.darray[0].get_metadata()["AnatomicalStructurePrimary"]
            surface = {"CortexLeft": "CORTEX_LEFT", "CortexRight": "CORTEX_RIGHT"}[primary_anat]
            # get the vertices
            vertices = list(np.argwhere(self._surface_file.agg_data() == 1)[:, 0])
            return (vertices, surface)

    def set_vertices(self, vertices: List[int], surface: str = None) -> None:
        """Sets the vertices for the Border object.

        Parameters
        ----------
        vertices : List[int]
            List of vertex indices to assign to the Border
        surface : str, optional
            Surface structure these vertices are on. If None, this will use the already stored value.
        """
        # set vertices and weights
        self._vertices.text = "\n".join([" ".join([str(n)] * 3) for n in vertices])
        self._weights.text = "\n".join(["0.33 0.33 0.33"] * len(vertices))

        # set the surface if it was defined
        if surface:
            self._borderfile.set("Structure", surface)

    @property
    def surface_name(self) -> str:
        """Return the surface name.

        Returns
        -------
        str
            Return the surface name this border is defined on.
        """
        return self._borderfile.get("Structure")

    @property
    def closed(self) -> bool:
        """Returns whether this is a closed border.

        Returns
        -------
        bool
            If True this is a closed border.
        """

        if self.mode == "border":
            return self._borderpart.get("Closed") == "True"
        elif self.mode == "surface":
            return False

    @property
    def mode(self) -> str:
        """Gets the mode of this border object.

        Returns
        -------
        str
            Can be "border" or "surface"
        """
        return self._mode

    def get_interpolated_vertices(self, cifti: Cifti, gifti: Gifti, method: str = "chenhan") -> Tuple[List[int], str]:
        """Gets a connected path for the stored vertices in this object.

        Parameters
        ----------
        cifti : Cifti
            A dtseries, dlabel, or dshape file with ROI information.
        gifti : Gifti
            A surface file containing surface geometry.
        method : str
            Method to use for interpolation (recommended is chenhan method.)

        Returns
        -------
        List[int]
            List of connected vertex indices
        str
            Surface name
        """
        # get the surface and the vertices to loop over
        vertices, surface_name = self.vertices

        # get surface graph from cifti/gifti
        if method != "chenhan":
            g = ROIGraph(cifti, gifti).graph

        # get the mode for the border object
        # if we loaded from a surface file, we need to order the vertices outselves
        # since the .gii has no sense of spatial ordering
        if self._mode == "surface":
            # use the lowest number vertex as the starting point
            ordered_vertices = [
                vertices[0],
            ]
            # initialize the current vertex
            current_vertex = vertices[0]
            # create a working set for the vertices list
            working_set = vertices[1:].copy()
            # loop over every other vertex in the working set, removing the vertex that is closest
            # to the current vertex from the working set and adding it to the ordered set, then
            # setting the current vertex to the recently added vertex
            # continue until the length of the working set is 1
            while len(working_set) > 1:
                best_distance = 999999999  # initialize to an arbitrary high number
                for index, proposed_vertex in enumerate(working_set):
                    proposed_distance = shortest_path_length(
                        g, source=current_vertex, target=proposed_vertex, weight="weight"
                    )
                    if proposed_distance < best_distance:
                        best_index = index
                        best_distance = proposed_distance
                # set the new vertex to the current vertex
                current_vertex = working_set[best_index]
                # add the best_index to the ordered_vertices
                ordered_vertices.append(working_set[best_index])
                working_set.remove(working_set[best_index])
            # ordered_vertices should be of len(vertices) - 1
            assert len(vertices) - 1 == len(ordered_vertices), "A fatal error has occurred!"
            # add the last vertex in the working set to the ordered set
            ordered_vertices.append(working_set[0])
            # now rename ordered_vertices to be vertex_list
            vertex_list = ordered_vertices
        elif self._mode == "border":
            # now loop over each pair of memory adjacent vertices and find the shortest
            # path between them to get a smooth traversal across the surface
            vertex_list = list()
            with yaspin(Spinners.dots12, text="Finding paths from given border vertices...", color="blue") as sp:
                for vbegin, vend in zip(vertices[:-1], vertices[1:]):
                    if method == "dijkstra":
                        path = shortest_path(g, source=vbegin, target=vend, weight="weight")
                    elif method == "chenhan":
                        path = chenhan_shortest_path(gifti.vertices, gifti.faces, vbegin, vend, True)
                    else:
                        raise ValueError("Unknown path method.")
                    sp.write("Found path from vertex %d to vertex %d." % (vbegin, vend))
                    vertex_list.append(vbegin)
                    vertex_list.extend(path[1:-1])
                # if the border is closed, we want to loop back to the beginning
                if self.closed:
                    if method == "dijkstra":
                        path = shortest_path(g, source=vertices[-1], target=vertices[0], weight="weight")
                    elif method == "chenhan":
                        path = chenhan_shortest_path(gifti.vertices, gifti.faces, vertices[-1], vertices[0], True)
                    else:
                        raise ValueError("Unknown path method.")
                    sp.write("Found path from vertex %d to vertex %d." % (vertices[-1], vertices[0]))
                    vertex_list.append(vertices[-1])
                    vertex_list.extend(path[1:-1])
                else:  # otherwise simply tag on the last vertex to the list
                    vertex_list.append(vertices[-1])
                sp.text = "All paths found."
                sp.ok("✅")

        # return the vertex list and surface name
        return (vertex_list, surface_name)

    def generate_path(
        self,
        cifti: Cifti,
        gifti: Gifti,
        index: int,
        counterclockwise: bool = True,
        cycles: float = 2,
        interpolate: bool = False,
        ptype: str = "spiral",
        reverse: bool = False,
    ) -> None:
        """Autogenerate a path for the borderfile for the given surface/cifti information

        Parameters
        ----------
        cifti : Cifti
            A dtseries, dlabel, or dshape file with ROI information.
        gifti : Gifti
            A surface file containing surface geometry.
        index : int
            [description]
        counterclockwise : bool, optional
            [description], by default True
        cycles : float, optional
            [description], by default 2
        interpolate : bool, optional
            [description], by default False
        ptype : str, optional
            [description], by default "spiral"
        reverse : bool, optional
            [description], by default False
        """
        # generate a graph from cifti, gifti data
        g = ROIGraph(cifti, gifti, index)
        if ptype == "spiral":
            path = g.get_spiral_path(counterclockwise, cycles, interpolate, reverse)
        elif ptype == "boundary":
            path = g.get_boundary_path(counterclockwise)

        # assign path to border object
        self.set_vertices(path, gifti.name)

    def write(self, filename: str) -> None:
        """Write Border to disk.

        Parameters
        ----------
        filename : str
            Filename to save borderfile as.
        """
        # create tree and write to disk
        self._btree.write(filename, encoding="UTF-8", xml_declaration=True)
