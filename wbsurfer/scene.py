from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import List, Tuple
import xml.etree.ElementTree as ET
from subprocess import CalledProcessError

import nibabel as nib
import numpy as np
from yaspin import yaspin
from yaspin.spinners import Spinners

from .images import Cifti, Gifti
from .utils import run_wbcommand_show_scene


class Scene:
    """Object representing a workbench scene."""

    def __init__(self, filename: str, scene_name: str, check_valid: bool = False) -> None:
        # get path to scene file
        self._path = Path(filename)

        # get name of scene
        self._name = scene_name

        # load the xml scene file
        self._scene_tree = ET.parse(filename)

        # get the root path for the current scene
        root_path = Path(self._path).parent.absolute()

        # loop over pathnames to change the root path
        path_names = self.root.findall(".//Object[@Type='pathName']")
        for path in path_names:
            if path.text is not None:
                if (root_path / path.text).exists():
                    path.text = (root_path / path.text).as_posix()

        # check if scene name is valid
        if check_valid:
            with yaspin(Spinners.dots12, text="Checking scene validity...", color="blue") as sp:
                try:
                    with NamedTemporaryFile() as f:
                        self.write_img(f.name + ".png")
                    sp.text = "Scene is valid."
                    sp.ok("✅")
                except CalledProcessError:
                    raise ValueError("Scene name is invalid!")

        # load the dconn
        try:
            dconn_path = self._get_dconn()
            self.cifti = Cifti(dconn_path)
        except IndexError:
            pass

        # load a dscalar
        try:
            parent_nodes = self.root.findall(self._tn_obj(end=".."))
            p_nodes = [p for p in parent_nodes if p.find(self._tn_obj(name="selectedMapFile")).text is not None]

            self.dscalar_nodes = [
                p.find(self._tn_obj(type="string", name="selectedMapName", end=".."))
                for p in p_nodes
                if ".dscalar.nii" in p.find(self._tn_obj(name="selectedMapFile")).text
            ]
            dscalar_path = self.dscalar_nodes[0].find(".//Object[@Name='selectedMapFileNameWithPath']").text
            self.cifti = Cifti(dscalar_path)
        except IndexError:
            pass

        # load a dtseries
        try:
            parent_nodes = self.root.findall(self._tn_obj(end=".."))
            p_nodes = [p for p in parent_nodes if p.find(self._tn_obj(name="selectedMapFile")).text is not None]

            self.dtseries_nodes = [
                p.find(self._tn_obj(type="string", name="selectedMapName", end=".."))
                for p in p_nodes
                if ".dtseries.nii" in p.find(self._tn_obj(name="selectedMapFile")).text
            ]
            dtseries_path = self.dtseries_nodes[0].find(".//Object[@Name='selectedMapFileNameWithPath']").text
            self.cifti_dtseries = Cifti(dtseries_path)
        except IndexError:
            pass

    @staticmethod
    def _tn_obj(type: str = "string", name: str = "selectedMapName", end: str = "") -> str:
        """Returns string for find/findall search"""
        return f".//Object[@Type='{type}'][@Name='{name}']{end}"

    @property
    def root(self) -> ET.Element:
        """Return the root node of the current scene."""
        # get the index of the selected scene
        scene_infos = self._scene_tree.findall(".//SceneInfo")
        current_index = None
        for info in scene_infos:
            if info.find(".//Name").text == self._name:
                current_index = info.get("Index")
                break

        # raise error if scene name not found
        if not current_index:
            raise ValueError("The scene name provided was not found in the given scene file!")

        # grab the root scene element for the current index
        root_scene = self._scene_tree.find(f".//Scene[@Index='{current_index}']")

        # return root scene element
        return root_scene

    def set_time_index(self, index: int) -> None:
        """Sets the time index for the cifti. Should be zero indexed."""
        # check if index is in bounds
        time_dim = self.cifti_dtseries.cifti.shape[0]
        assert (
            index >= 0 and index < time_dim
        ), "Uh oh. You gave an index that exceeded the time dimension of the dtseries!"

        # loop over dtseries nodes and set updated index
        for dt_node in self.dtseries_nodes:
            mapname = dt_node.find(".//Object[@Name='selectedMapName']")
            mapindex = dt_node.find(".//Object[@Name='selectedMapIndex']")
            mapname.text = f"{index+1} seconds"
            mapindex.text = str(index)

    def _get_row_node(self) -> ET.Element:
        """Returns the m_rowIndex node."""
        return self.root.find(self._tn_obj("integer", "m_rowIndex"))

    def _get_dconn_nodes(self, type: str = "string", name: str = "selectedMapName") -> List[ET.Element]:
        """Return a list of dconn nodes."""
        # get all the parent nodes of all selectedMapName
        parent_nodes = self.root.findall(self._tn_obj(end=".."))

        # get only nodes with defined selectedMapFile
        p_nodes = [p for p in parent_nodes if p.find(self._tn_obj(name="selectedMapFile")).text is not None]

        # return list of dconn nodes
        return [
            p.find(self._tn_obj(type=type, name=name))
            for p in p_nodes
            if ".dconn.nii" in p.find(self._tn_obj(name="selectedMapFile")).text
        ]

    def _get_row(self) -> int:
        """This gets the row index from the m_rowIndex node."""
        return int(self._get_row_node().text)

    def _get_row_vertex(self) -> Tuple[int, int]:
        """This gets the row and vertex indices from the dconn nodes."""
        # from the parent node of the first dconn file, grab the text from selectedMapName
        map_name_str = self._get_dconn_nodes()[0].text

        # parse the row and vertex indices
        row_idx = int(map_name_str.split("Row: ")[1].split(",")[0]) - 1
        vertex_idx = int(map_name_str.split("Vertex Index: ")[1].split(",")[0])
        surface = map_name_str.split("Structure: ")[1]

        # return the row and vertex indices
        return (row_idx, vertex_idx, surface)

    def _get_dconn(self) -> nib.Cifti2Image:
        """Get the dconn image."""
        # get the path
        dconn_node = self._get_dconn_nodes("pathName", "selectedMapFileNameWithPath")[0]

        # construct path relative to scene file
        dconn_path = self._path.absolute().parent / dconn_node.text

        # return dconn path
        return dconn_path

    def get_surf(self, surface_name: str) -> Gifti:
        """Get the specified surface"""
        # grab all Objects of Class BrainStructure
        bs_objs = self.root.findall(".//Object[@Type='class'][@Class='BrainStructure']")

        # sort out which object is right/left cortex
        surface_paths = list()
        for surf_name in ["CORTEX_LEFT", "CORTEX_RIGHT"]:
            for obj in bs_objs:
                if obj.find("./Object[@Type='enumeratedType'][@Name='m_structure']").text == surf_name:
                    surface_paths.append(obj.find("./Object[@Type='pathName'][@Name='primaryAnatomicalSurface']").text)
                    break

        # load surfaces
        if surface_name == "CORTEX_LEFT":
            return Gifti(surface_paths[0])
        elif surface_name == "CORTEX_RIGHT":
            return Gifti(surface_paths[1])

    def get_vertex_row_table(self) -> Tuple[np.ndarray, int, int, np.ndarray, int, int]:
        """Get vertex row table."""
        return self.cifti.get_vertex_row_table()

    def _vertex_to_row_index(self, vertex_idx: int, surface: str = "CORTEX_LEFT") -> int:
        """Computes row index from vertex index."""
        return self.cifti.vertex_to_row_index(vertex_idx, surface)

    def _row_to_vertex_index(self, row_idx: int) -> Tuple[int, str]:
        """Computes vertex index from row index."""
        return self.cifti.row_to_vertex_index(row_idx)

    def _set_row_vertex(self, row: int, vertex: int, surface: str) -> None:
        """Set row/vertex of scene."""
        # get current row/vertex/surface
        current_row, current_vertex, current_surface = self._get_row_vertex()

        # set all vertex nodes in scene file
        vertex_nodes = self.root.findall(".//ObjectArray[@Type='integer'][@Name='m_surfaceNodeIndices']")
        for vertex_node in vertex_nodes:
            vertex_element = vertex_node.find(".//Element")
            vertex_element.text = str(vertex)

        # set surface nodes in scene file
        surface_nodes = self.root.findall(".//Object[@Type='enumeratedType'][@Name='m_surfaceStructure']")
        for surface_node in surface_nodes:
            surface_node.text = surface

        # set all row nodes in scene file
        row_nodes = self.root.findall(".//Object[@Type='integer'][@Name='m_rowIndex']")
        for row_node in row_nodes:
            row_node.text = str(row)

        # set dconn nodes
        nodes = self._get_dconn_nodes()
        for n in nodes:
            n.text = f"Row: {row + 1}, Vertex Index: {vertex}, Structure: {surface}"

        # TODO: Does the history record really need to be set???
        surf_dict = {"CORTEX_LEFT": "CortexLeft", "CORTEX_RIGHT": "CortexRight"}

        # set history_records
        history_records = self.root.findall(".//Object[@Type='class'][@Name='historyRecord']")
        for history_record in history_records:
            hr_string = history_record.find(".//Object[@Type='string'][@Name='m_text']")
            hr_text = hr_string.text
            hr_string.text = (
                hr_text.replace(f"VERTEX {current_vertex}", f"VERTEX {vertex}")
                .replace(f"Row {current_row + 1}", f"Row {row + 1}")
                .replace(f"Column {current_row + 1}", f"Column {row + 1}")
                .replace(f"Row Index: {current_row + 1}", f"Row Index: {row + 1}")
                .replace(surf_dict[current_surface], surf_dict[surface])
            )

        # set identified item
        identified_items = self.root.findall(
            ".//Object[@Type='class'][@Class='IdentifiedItemNode'][@Name='identifiedItem']"
        )
        for identified_item in identified_items:
            m_structure = identified_item.find(".//Object[@Type='enumeratedType'][@Name='m_structure']")
            m_structure.text = surface
            m_node_index = identified_item.find(".//Object[@Type='integer'][@Name='m_nodeIndex']")
            m_node_index.text = str(vertex)
            identified_string = identified_item.find(".//Object[@Type='string'][@Name='m_text']")
            identified_text = identified_string.text
            identified_string.text = (
                identified_text.replace(f"VERTEX {surf_dict[current_surface]}", f"VERTEX {surf_dict[surface]}")
                .replace(str(current_row + 1), str(row + 1))
                .replace(str(current_vertex), str(vertex))
            )
            formatted_string = identified_item.find(".//Object[@Type='string'][@Name='m_formattedText']")
            if formatted_string:
                formatted_text = formatted_string.text
                formatted_string.text = (
                    formatted_text.replace(f"VERTEX {current_vertex}", f"VERTEX {vertex}")
                    .replace(surf_dict[current_surface], surf_dict[surface])
                    .replace(f"Row {current_row + 1}", f"Row {row + 1}")
                    .replace(f"Column {current_row + 1}", f"Column {row + 1}")
                    .replace(f"Row Index: {current_row + 1}", f"Row Index: {row + 1}")
                )

    @property
    def current_row(self) -> int:
        """Returns the current row of the scene"""
        assert self._get_row_vertex()[0] == self._get_row(), "Rows from dconn nodes vs. m_rowIndex inconsistent!"
        return self._get_row()

    @current_row.setter
    def current_row(self, row: int) -> None:
        """Sets the current row."""
        # get vertex from row
        vertex, surface = self._row_to_vertex_index(row)

        # set row/vertex/surface
        self._set_row_vertex(row, vertex, surface)

    @property
    def current_vertex(self) -> Tuple[int, str]:
        """Returns the current vertex of the scene"""
        return self._get_row_vertex()[1:3]

    @current_vertex.setter
    def current_vertex(self, value: Tuple[int, str]) -> None:
        """Sets the current vertex."""
        # get row from vertex
        vertex = value[0]
        surface = value[1]
        row = self._vertex_to_row_index(vertex, surface)

        # set row/vertex/surface
        self._set_row_vertex(row, vertex, surface)

    @property
    def valid_rows(self) -> Tuple[int, int, int, int]:
        """Return valid rows."""
        table = self.get_vertex_row_table()
        return (table[1], table[2], table[4], table[5])

    def write(self, filename: str) -> None:
        """Write scene to file."""
        self._scene_tree.write(filename)

    def write_img(self, filename: str, width: int = 1920, height: int = 1080, capture_output: bool = True) -> None:
        """Write scene to image."""
        # create scene file path
        scene_file = filename.split(".png")[0] + ".scene"

        # write out scene
        self.write(scene_file)

        # convert scene to image
        run_wbcommand_show_scene(scene_file, self._name, filename, width, height, capture_output)

    def write_img_row(
        self, row: int, path: str, width: int = 1920, height: int = 1080, capture_output: bool = True, index: int = None
    ) -> None:
        """Write write out selected row for scene in image."""
        path = Path(path)
        self.current_row = row
        file_prefix = f"{index:09d}" if index is not None else f"{row:09d}"
        filename = (path / (file_prefix + ".png")).as_posix()
        self.write_img(filename, width, height, capture_output)

    def write_img_vertex(
        self,
        vertex: int,
        surface: str,
        path: str,
        width: int = 1920,
        height: int = 1080,
        capture_output: bool = True,
        index: int = None,
    ) -> None:
        """Write write out selected vertex for scene in image."""
        path = Path(path)
        self.current_vertex = (vertex, surface)
        file_prefix = f"{index:09d}" if index is not None else f"{vertex:09d}"
        filename = (path / (file_prefix + ".png")).as_posix()
        self.write_img(filename, width, height, capture_output)
